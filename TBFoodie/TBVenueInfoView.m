//
//  TBVenueInfoView.m
//  TBFoodie
//
//  Created by Elsa Filippidou on 28/08/16.
//  Copyright © 2016 Elsa Filippidou. All rights reserved.
//

#import "TBVenueInfoView.h"
#import "TBVariablesAndMacros.h"
#import "UIColor+HexString.h"

@implementation TBVenueInfoView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    [self.venueName setTextColor:[UIColor colorWithHexString:TBColorBrown]];
    [self.venuePoints setTextColor:[UIColor colorWithHexString:TBColorSalmonDark]];
    [self.venueAddressLabel setTextColor:[UIColor colorWithHexString:TBColorBrown]];
    [self.venueCategoryLabel setTextColor:[UIColor colorWithHexString:TBColorSalmonDark]];
    
    [self.noImageLabel setText:@"No image"];
    
    // add border
    CGFloat borderWidth = 5.0f;
    self.frame = CGRectInset(self.frame, -borderWidth, -borderWidth);
    self.layer.borderColor = [rgbColor(200, 200, 200) CGColor];
    self.layer.borderWidth = borderWidth;
    self.layer.cornerRadius = 5.0f;
}

- (void)checkImage {
    
    if (self.venueImageView.image == nil) {
        [_noImageLabel setHidden:FALSE];
    }
    else {
        [_noImageLabel setHidden:TRUE];
    }
}

@end
