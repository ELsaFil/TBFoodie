//
//  TBFooterOverView.h
//  TBFoodie
//
//  Created by Elsa Filippidou on 27/08/16.
//  Copyright © 2016 Elsa Filippidou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBFooterOverView : UIView

@property (nonatomic, strong) IBOutlet UILabel *currentLocationLabel;
@property (nonatomic, strong) IBOutlet UIImageView *userIconIV;

@end
