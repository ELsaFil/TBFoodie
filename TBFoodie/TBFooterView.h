//
//  TBFooterView.h
//  TBFoodie
//
//  Created by Elsa Filippidou on 27/08/16.
//  Copyright © 2016 Elsa Filippidou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBFooterView : UIView

@property (nonatomic, strong) IBOutlet UILabel *locationLabel;

@end
