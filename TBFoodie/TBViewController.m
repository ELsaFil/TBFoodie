//
//  TBViewController.m
//  TBFoodie
//
//  Created by Elsa Filippidou on 27/08/16.
//  Copyright © 2016 Elsa Filippidou. All rights reserved.
//

#import "TBViewController.h"
#import "TBVariablesAndMacros.h"
#import "UIColor+HexString.h"
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "TBVenueModel.h"

@interface TBViewController () <CLLocationManagerDelegate, GMSMapViewDelegate> {
    
    GMSGeocoder *geocoder;
    CLLocationManager *locationManager;
    GMSMapView *theMapView;
    GMSMarker *marker;
    
    NSMutableArray *allMarkers;
}

@end

@implementation TBViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    allMarkers = [NSMutableArray new];
    
    [_headerView  setBackgroundColor:[[UIColor colorWithHexString:TBColorTurquoiseDark] colorWithAlphaComponent:0.9]];
    
    [_footerOverView setBackgroundColor:[rgbColor(200, 200, 200) colorWithAlphaComponent:0.7]];
    
    [_footerView setBackgroundColor:[UIColor colorWithHexString:TBColorBrown]];
    
    geocoder = [[GMSGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    
    // setup the map view - start with default location - Athens
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:TBDefaultLat longitude:TBDefaultLon zoom:TBDefaultZoom];
    theMapView = [GMSMapView mapWithFrame:self.mapView.frame camera:camera];
    theMapView.myLocationEnabled = NO;
    theMapView.accessibilityElementsHidden = NO;
    theMapView.settings.compassButton = NO;
    theMapView.settings.myLocationButton = YES;
    theMapView.mapType = kGMSTypeNormal;
    theMapView.delegate = self;
    
    [self.view addSubview:theMapView];
    [self.view sendSubviewToBack:theMapView];
    [self.mapView removeFromSuperview];
    
    marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(TBDefaultLat, TBDefaultLon);
    marker.title = TBDefaultMapTitle;
    marker.icon = [UIImage imageNamed:@"icon-me-pin"];
    marker.snippet = TBDefaultMapSnippet;
    marker.map = theMapView;
    
    // hide the info view
    _infoView.hidden = TRUE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    
    _infoView.hidden = TRUE;
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture {
    //[mapView clear];
    //_infoView.hidden = TRUE;
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)aMarker {
    
    for (GMSMarker *anyMarker in allMarkers) {
        anyMarker.icon = [UIImage imageNamed:@"icon-venue"];
    }
    
    aMarker.icon = [UIImage imageNamed:@"icon-venue-selected"];
    
    [_infoView.venueName setText:aMarker.title];
    [_infoView.venueAddressLabel setText:aMarker.snippet];
    [_infoView.venueCategoryLabel setText:@""];
    [_infoView.venuePoints setText:@""];
    [_infoView setHidden:FALSE];
    [_infoView checkImage];
    
    return YES;
}

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position {
    
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)cameraPosition {
    
    CLLocationCoordinate2D theLoc = cameraPosition.target;
    
    // get Foursquare locations
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self getDataForLocation:theLoc];
    });
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"Error" message:@"Failed to get current location." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *alertOkAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alertC addAction:alertOkAction];
    
    [self presentViewController:alertC animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    if (![locations count]){
        return; // prevent crash
    }
        
    CLLocation *currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    
    NSLog(@"didUpdateToLocation: %@", currentLocation);
    
    [self->locationManager stopUpdatingLocation];
    
    float nLocationLat = currentLocation.coordinate.latitude;
    float nLocationLon = currentLocation.coordinate.longitude;
    
    if (currentLocation != nil) {
        
        GMSCameraPosition *currentCameraPos = [GMSCameraPosition cameraWithLatitude:nLocationLat
                                                                       longitude:nLocationLon
                                                                            zoom:TBDefaultZoom];
        [theMapView setCamera:currentCameraPos];
        
        // Creates a marker in the center of the map.
        marker.position = CLLocationCoordinate2DMake(nLocationLat, nLocationLon);
        marker.icon = [UIImage imageNamed:@"icon-me-pin"];
        marker.map = theMapView;
        
        id handler = ^(GMSReverseGeocodeResponse *response, NSError *error) {
            if (error == nil) {
                GMSReverseGeocodeResult *result = response.firstResult;
                marker.title = result.lines[0];
                marker.snippet = result.lines[1];
                
                self.footerView.locationLabel.text = marker.title;
            }
        };
        [geocoder reverseGeocodeCoordinate:currentCameraPos.target completionHandler:handler];
        
        // get Foursquare locations here
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self getDataForLocation:currentCameraPos.target];
        });
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        
        [locationManager startUpdatingLocation];
        
        theMapView.myLocationEnabled = NO;
        theMapView.settings.myLocationButton = NO;
    }
}

#pragma mark Private methods

- (void) getDataForLocation:(CLLocationCoordinate2D)queryLocation {
    
    float queryLat = queryLocation.latitude;
    float queryLon = queryLocation.longitude;
    
    NSString *searchURL = @"http://api.foursquare.com/v2/venues/search?";
    NSString *searchKeys = [NSString stringWithFormat:@"client_id=%@&client_secret=%@",TBAuthClientIDFoursquare,TBAuthClientSecretFoursquare];
    NSString *searchQuery = @"bakery";
    NSString *searchParams = [NSString stringWithFormat:@"&v=20130815&near=%f,%f&query=%@&limit=50&intent=browse&radius=1000", queryLat, queryLon, searchQuery];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",searchURL, searchKeys, searchParams]];
    NSURLSession *session = [NSURLSession sharedSession];
    
    // request data, wait until completed, use the data
    NSURLSessionDataTask *task = [session dataTaskWithURL:url
                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *connectionError) {
                                            NSError *error;
                                            NSDictionary *locationsResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                            NSArray *locationsDict = [[locationsResult objectForKey:@"response"] objectForKey:@"venues"];
                                            
                                            if ([locationsDict count]) {
                                                
                                                NSLog(@"Found %i bakeries!", (int)[locationsDict count]);
                                                
                                                // break down API data
                                                for (int i=0; i < locationsDict.count; i++) {
                                                    
                                                    NSDictionary *aVenueDict = [locationsDict objectAtIndex:0];
                                                    
                                                    // create venue object
                                                    TBVenueModel *aVenue = [[TBVenueModel alloc] init];
                                                    aVenue.venueName = [aVenueDict objectForKey:@"name"];
                                                    aVenue.address = [[aVenueDict objectForKey:@"location"] objectForKey:@"address"];
                                                    aVenue.locationLat = [[[aVenueDict objectForKey:@"location"] objectForKey:@"lat"] floatValue];
                                                    aVenue.locationLon = [[[aVenueDict objectForKey:@"location"] objectForKey:@"lng"] floatValue];
                                                    aVenue.postalCode = [[aVenueDict objectForKey:@"location"] objectForKey:@"postalCode"];
                                                    aVenue.country = [[aVenueDict objectForKey:@"location"] objectForKey:@"country"];
                                                    aVenue.city = [[aVenueDict objectForKey:@"location"] objectForKey:@"city"];
                                                    aVenue.category = [[[aVenueDict objectForKey:@"categories"] objectAtIndex:0] objectForKey:@"name"];
                                                    
                                                    // update UI in main thread
                                                    dispatch_sync(dispatch_get_main_queue(), ^{
                                                        
                                                        GMSMarker *nLocMarker = [[GMSMarker alloc] init];
                                                        nLocMarker.position = CLLocationCoordinate2DMake(aVenue.locationLat, aVenue.locationLon);
                                                        nLocMarker.title = aVenue.venueName;
                                                        nLocMarker.snippet = aVenue.address;
                                                        nLocMarker.icon = [UIImage imageNamed:@"icon-venue"];
                                                        nLocMarker.map = theMapView;
                                                        
                                                        aVenue.marker = nLocMarker;
                                                        
                                                        [allMarkers addObject:nLocMarker];
                                                    });
                                                }
                                            }
                                            else {
                                                NSLog(@"No bakeries in this area.");
                                            }
                                        }
                                  ];
    [task resume];
}

@end