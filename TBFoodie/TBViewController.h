//
//  TBViewController.h
//  TBFoodie
//
//  Created by Elsa Filippidou on 27/08/16.
//  Copyright © 2016 Elsa Filippidou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TBHeaderView.h"
#import "TBMapView.h"
#import "TBFooterOverView.h"
#import "TBFooterView.h"
#import "TBVenueInfoView.h"

@interface TBViewController : UIViewController

@property (strong, nonatomic) IBOutlet TBHeaderView *headerView;
@property (strong, nonatomic) IBOutlet TBMapView *mapView;
@property (strong, nonatomic) IBOutlet TBFooterOverView *footerOverView;
@property (strong, nonatomic) IBOutlet TBFooterView *footerView;

@property (weak, nonatomic) IBOutlet TBVenueInfoView *infoView;

@end

