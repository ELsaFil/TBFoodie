//
//  TBFooterView.m
//  TBFoodie
//
//  Created by Elsa Filippidou on 27/08/16.
//  Copyright © 2016 Elsa Filippidou. All rights reserved.
//

#import "TBFooterView.h"

@implementation TBFooterView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    [self.locationLabel setTextColor:[UIColor whiteColor]];
}


@end
