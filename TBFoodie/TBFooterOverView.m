//
//  TBFooterOverView.m
//  TBFoodie
//
//  Created by Elsa Filippidou on 27/08/16.
//  Copyright © 2016 Elsa Filippidou. All rights reserved.
//

#import "TBFooterOverView.h"
#import "TBVariablesAndMacros.h"
#import "UIColor+HexString.h"

@implementation TBFooterOverView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    [self.currentLocationLabel setTextColor:[UIColor colorWithHexString:TBColorSalmonDark]];
    [self.currentLocationLabel setText:@"CURRENT LOCATION"];
}
 

@end
