//
//  TBVenueInfoView.h
//  TBFoodie
//
//  Created by Elsa Filippidou on 28/08/16.
//  Copyright © 2016 Elsa Filippidou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBVenueInfoView : UIView

@property (nonatomic, strong) IBOutlet UILabel *venueName;
@property (nonatomic, strong) IBOutlet UILabel *venuePoints;
@property (nonatomic, strong) IBOutlet UILabel *venueAddressLabel;
@property (nonatomic, strong) IBOutlet UILabel *venueCategoryLabel;

@property (nonatomic, strong) IBOutlet UILabel *noImageLabel;
@property (nonatomic, strong) IBOutlet UIImageView *venueImageView;

- (void)checkImage;

@end
