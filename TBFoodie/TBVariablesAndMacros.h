//
//  TBVariablesAndMacros.h
//  TBFoodie
//
//  Created by Elsa Filippidou on 27/08/16.
//  Copyright © 2016 Elsa Filippidou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define rgbColor(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]

// authentication
extern NSString *const TBAPIKey;
extern NSString *const TBAuthClientIDFoursquare;
extern NSString *const TBAuthClientSecretFoursquare;

// map properties
extern float const TBDefaultLat;
extern float const TBDefaultLon;
extern float const TBDefaultZoom;
extern NSString *const TBDefaultMapTitle;
extern NSString *const TBDefaultMapSnippet;


// colors
extern NSString *const TBColorTurquoise;
extern NSString *const TBColorTurquoiseDark;
extern NSString *const TBColorAquamarine;
extern NSString *const TBColorBrown;
extern NSString *const TBColorSalmonDark;


@interface TBVariablesAndMacros : NSObject

@end
