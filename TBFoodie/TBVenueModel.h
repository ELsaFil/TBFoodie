//
//  TBVenueModel.h
//  TBFoodie
//
//  Created by Elsa Filippidou on 28/08/16.
//  Copyright © 2016 Elsa Filippidou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface TBVenueModel : NSObject

@property (strong, nonatomic) NSString *venueName;
@property (strong, nonatomic) NSString *address;
@property (assign, nonatomic) float locationLat;
@property (assign, nonatomic) float locationLon;
@property (strong, nonatomic) NSString *postalCode;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *country;

@property (strong, nonatomic) NSString *category;
@property (strong, nonatomic) NSString *categoryIconPrefix;
@property (strong, nonatomic) NSString *categoryIconSuffix;

@property (strong, nonatomic) GMSMarker *marker;

@end
