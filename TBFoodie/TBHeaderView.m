//
//  TBHeaderView.m
//  TBFoodie
//
//  Created by Elsa Filippidou on 27/08/16.
//  Copyright © 2016 Elsa Filippidou. All rights reserved.
//

#import "TBHeaderView.h"

@implementation TBHeaderView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    [self.titleLabel setTextColor:[UIColor whiteColor]];
    [self.titleLabel setText:@"Foodie"];
}

@end
