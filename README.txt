*************

TBFoodie

Created by: Elsa Filippidou
August 2016

*************

TBFoodie is an app created as an exercise. 
It implements Google Maps SDK for iOS and Foursquare API for getting a list of venues. Feel free to use any of the code in this app.

The assets belong to Taxibeat (taxibeat.com).


